Créer un programme qui demande à l'utilisateur de saisir un texte.
Afficher ce texte dans un label. Dès que l'utilisateur change sa saisie de texte, le label doit changer tout de suite pour se mettre à jour par rapport à la saisie.

Faire les boutons Rouge, Bleu, Vert, Jaune, Rose, Marron.
Au clique sur un bouton, le label doit changer avec la couleur associée au bouton.
