from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
import kivy.properties as kProperties
from kivy.uix.button import Button
from kivy.config import Config
from kivy.uix.scrollview import ScrollView


class MainScreen(FloatLayout):
    __slots__ = ("_text", "_label", "_buttons", "_scroll")

    def __init__(self, **kwargs):
        self.size = (Config.get('graphics', 'width'), Config.get('graphics', 'height'))
        self.oldSize = self.size
        super().__init__(**kwargs)
        self._text = self.createTextInput(self, x=0, y=0.8, width=1, height=0.2)
        self._scroll = self.createScrollView(self, x=0, y=.6, width=1, height=.2)
        self._label = self.createLabel(self._scroll, x=0, y=1, width=1, height=None)
        self._buttons = self.createButtons()  # Ca sert à rien de les stocker, mais bon, sait-on jamais...

    def createTextInput(self, parent, x: float, y: float, width: float, height: float):
        t = TextInput(size_hint=(width, height), pos_hint={"x": x, "y": y})
        parent.add_widget(t)
        t.bind(text=self.refreshText)
        return t

    def createScrollView(self, parent, x: float, y: float, width: float, height: float):
        scroll = ScrollView(size_hint=(width, height), pos_hint={"x": x, "y": y}, do_scroll_x=False,
                            do_scroll_y=True, scroll_type=["bars", "content"])
        parent.add_widget(scroll)
        return scroll

    def createLabel(self, parent, x: float, y: float, width: float, height: [float, None]):
        h = height
        if height is not None:
            h = height * self.size[1]
        label = Label(text="Ceci est un Label", pos_hint={"x": x, "y": y}, size_hint=(width, height),
                      color=(1, 1, 1, 1), text_size=(width * self.size[0], h), valign='center',
                      halign='center')
        parent.add_widget(label)
        return label

    def createButtons(self):
        buttons = [
            self.createButton(self, "Rouge", (1, 0, 0), 0, 0.3, 0.335, 0.3),
            self.createButton(self, "Bleu", (0, 0, 1), 0.335, 0.3, 0.335, 0.3),
            self.createButton(self, "Vert", (0, 1, 0), 0.67, 0.3, 0.335, 0.3),
            self.createButton(self, "Jaune", (1, 1, 0), 0, 0, 0.335, 0.3),
            self.createButton(self, "Rose", (1, 0, 1), 0.335, 0, 0.335, 0.3),
            self.createButton(self, "Marron", (0.5, 0, 0), 0.67, 0, 0.335, 0.3)
        ]
        return buttons.copy()

    def createButton(self, parent, name: str, color: tuple, x: float, y: float, width: float, height: float):
        cb = CustomBtn(text=name, color=color, pos_hint={"x": x, "y": y}, size_hint=(width, height))
        cb.bind(pressed=self.changeColor)
        parent.add_widget(cb)
        return cb

    def text(self):
        return self._text

    def label(self):
        return self._label

    def set_text(self, txt: str):
        self._text = txt

    def set_label(self, label):
        self._label = label

    def set_label_text(self, txt: str):
        self.label().text = txt

    def set_label_color(self, color: tuple):
        self.label().color = color

    def refreshText(self, instance, text):
        self.set_label_text(text)

    def changeColor(self, instance, boolean):
        if boolean:
            self.set_label_color(instance.color)


class CustomBtn(Button):
    pressed = kProperties.BooleanProperty(False)

    def on_touch_down(self, touch):
        self.pressed = self.collide_point(*touch.pos)
        super().on_touch_down(touch)

    def on_touch_up(self, touch):
        self.pressed = False
        self.background_color = (1, 1, 1, 1)
        super().on_touch_up(touch)

    def on_pressed(self, instance, boolean):
        self.background_color = self.color
        self.background_down = self.background_normal


class MyApp(App):
    def build(self):
        self.title = "Changement de couleurs"
        return MainScreen()


def setConfig():
    Config.set('graphics', 'minimum_height', 200)
    Config.set('graphics', 'minimum_width', 200)
    Config.set('graphics', 'height', 720)
    Config.set('graphics', 'width', 1280)
    Config.set('graphics', 'resizable', 1)
    Config.set('graphics', 'fullscreen', 0)
    Config.write()


if __name__ == '__main__':
    setConfig()
    MyApp().run()
